import geoip2.database


class GeoCode(object):

    def __init__(self,geofile1,geofile2):

        self.city_reader = geoip2.database.Reader(geofile1)
        self.asn_reader = geoip2.database.Reader(geofile2)

    def geocode(self,ip):
        asn_response = self.asn_reader.asn(ip)
        city_response = self.city_reader.city(ip)
        self.country = city_response.country.name
        self.city = city_response.city.name
        self.latitude = city_response.location.latitude
        self.longitude = city_response.location.longitude
        self.asn = asn_response.autonomous_system_organization
        return self


'''
with geoip2.database.Reader('/path/to/GeoLite2-ASN.mmdb') as reader:
     response = reader.asn('1.128.0.0')
     response.autonomous_system_number
1221
     response.autonomous_system_organization
reader = geoip2.database.Reader('GeoLite2-City.mmdb')

# replace city with the method corresponding to the database
response = reader.city('128.101.101.101')

print(response.country.name)
print(response.city.name)
print(response.location.latitude)
print(response.location.longitude)

n = GeoCode('GeoLite2-City.mmdb')
n.geocode('128.101.101.101')
print(n.city)
'''
