
function myAutocomplete() {
  let inp = document.getElementById('myInput');
  let arr = [    
                 'smb',
                 'mssql',
                 'http',
                 'ssh',
                 'upnp',
                 'sip',
                 'ftp',
                 'mysql',
                 'pptp',
                 'tftp',
                 'mqtt',
                 'mongodb'
             ];
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus = -1;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";

          /*execute a function when someone clicks on the item value (DIV element):*/
              b.addEventListener("click", function(e) {
                // check value of checkbox for wantsLines
                let wantsLines = document.getElementById('wantsLines').checked;
                read(this.getElementsByTagName("input")[0].value, wantsLines);
                closeAllLists();
              });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        
        if (currentFocus > -1) {
          //and simulate a click on the "active" item:
          if (x) x[currentFocus].click();
        }
        
        ;
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
    //console.log(x[currentFocus].classList);
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
      x[i].parentNode.removeChild(x[i]);
    }
    
  }
}
/*execute a function when someone clicks in the document:*/
document.addEventListener("click", function (e) {
    closeAllLists(e.target);
});
}


function drawMap(data1,lines){

        //let oldsvg = d3.select('#bot_display');
        //oldsvg.selectAll("*").remove();
        let data = data1.data;
        //console.log(data['nyc3'].device_info);
        // data.device 'nyc3' etc defines color

	var w = 1000;
	var h = 700;

	//Create SVG element
	var svg = d3.select("#bot_display")
				.append("svg")
				.attr("width", w)
				.attr("height", h);

	var projection = d3.geoMercator().scale(w / 2 / Math.PI).translate([w/2, h/2]);


	//Define path generator
	var path = d3.geoPath().projection(projection);

	//Load in GeoJSON data
	var url = "http://enjalot.github.io/wwsd/data/world/world-110m.geojson";
	d3.json(url, function(json) {
		
                // draw world map
		svg.selectAll("path")
		   .data(json.features)
		   .enter()
		   .append("path")
		   .attr("d", path)
                   .attr("id", "mercator");

                var legend = svg.append("g")
                  .attr("class", "legend")
                  .attr("x", 0)
                  .attr("y", 0)
                  .attr("stroke","white")
                  .attr("height", 200)
                  .attr("width", 200);    

                // for loop over sensor names and draw circles and pot for each set
                let sensors = ['nyc3','sgp1'];
                let legend_y = 250;
                sensors.forEach(function(sensor){
                   console.log(data[sensor].longitude);
                   let color_map = {'nyc3':'#ea07d7','blr1':'green','sgp1':'#7cea07','fra1':'purple'};
                   let color = color_map[data[sensor].device];         
                   let pot_x = projection([data[sensor].longitude,data[sensor].latitude])[0];
                   let pot_y = projection([data[sensor].longitude,data[sensor].latitude])[1];
                   let pot_object = {"latitude":pot_x,"longitude":pot_y};

                   data[sensor].logs.forEach((d)=>{

		     svg.append("circle")
		     .attr("cx", projection([d.longitude,d.latitude])[0])
		     .attr("cy", projection([d.longitude,d.latitude])[1])
		     .attr("r", ()=>{
				return Math.log(d.count * 10);
		     })
                     .attr("fill", color)
		     .attr("stroke-width", ()=>{
				return Math.log(d.count * 10);
		     })
                     .attr("stroke-opacity","0.1")
                     .attr("stroke","black")
		     .append("title")			
		     .text(()=>{
                                
                       if (d.city){
		         return `${d.city},${d.country}\n${d.count}\n${d.asn}\n${d.src_ip}`;
                       } else {
		         return `${d.country}\n${d.count}\n${d.asn}\n${d.src_ip}`;
                       }
		     });

                      if (lines){
                        svg.append('line')
		        .attr("x1", pot_x)
		        .attr("y1", pot_y)
		        .attr("x2", projection([d.longitude,d.latitude])[0])
		        .attr("y2", projection([d.longitude,d.latitude])[1])
		        .attr("stroke-width", ".25")
		        .attr("stroke", color);
                      }

                   });

		   svg.append("circle")
		     .attr("cx", pot_x)
		     .attr("cy", pot_y)
                     .attr("r", "10")
                     .attr("fill", "yellow")
                     .attr("stroke",color)
                     .attr("stroke-opacity","1")
                     .attr("stroke-width","5");
                   
                   // ------------- make legend ---------------------
 
                   legend.append("circle")
                    .attr("fill", "yellow")
                    .attr("cx", 100)
                    .attr("cy", function(){ return legend_y += 30;})
                    .attr("r","10")
                    .attr("width", 10)
                    .attr("height", 10)
                    .attr("stroke",color)
                    .attr("stroke-width","5");

                  legend.append("text")
                    .attr("x", 10)
                    .attr("y", function(){ return legend_y += 35;})
                    .attr("class","legend_svg")
		    .text(()=>{
                       //return "hello there";
                       if (data[sensor].city){
		         return `sensor in ${data[sensor].city},${data[sensor].country}`;
                       } else {
		         return `sensor in ${data[sensor].country}`;
                       }
		    });
                  // ----------- end legend ----------------                    
                
                });
        });
}


function read(t,wantsLines){

  document.getElementById("myTicker").style.opacity = 0;
  document.getElementById('myTicker').innerHTML = "";
  let oldsvg = d3.select('#bot_display');
  oldsvg.selectAll("*").remove();
  clearTicker();
  document.getElementById('lds-container').style.opacity = "1";
  
  //let t = document.getElementById("myInput").value;
  if (!t) t = "ssh";

  // need to sanitize input here  

  let mapping = {'http':'httpd',
                 'mssql':'mssqld',
                 'http':'httpd',
                 'ssh':'ssh',
                 'upnp':'upnpd',
                 'sip':'SipSession',
                 'ftp':'ftpd',
                 'mysql':'mysqld',
                 'pptp':'pptpd',
                 'tftp':'tftpd',
                 'mqtt':'mqttd',
                 'smb':'smbd',
                 'mongodb':'mongodbd'
                 };
  let val = mapping[t];
  axios.get(`http://spottedbot.com:4000/api/data/${val}`
  )
  .then((response)=>{

      //console.log(response);   

      if (response.data){
        console.log(response.data.data);
        //computeRatio(response.data,t);
        showMax(response.data,t);
        showDayCount(response.data,t);
        drawMap(response.data, wantsLines);
        document.getElementById('myInput').value = '';
        document.getElementById('lds-container').style.opacity = "0";
        showTicker();
      } else {
        document.getElementById('myInput').value = 'no current results';

      }
  })
  .catch((error)=>{

      document.getElementById('myInput').value = 'no current results';
 
  })
}

function computeRatio(data,type){
    //childList.removeChild(childList.childNodes); 
    let attackType = type;

    for (let sensor in data.data){
      let movement = "decreased";
      let comment = '';
      // if day_count is zero, comment is 'no movement' 
      // if pervious day count 
      if (data.data[sensor].day_count !== 0){
        let diff = data.data[sensor].previous_day_count - data.data[sensor].day_count;
        //console.log(diff);
        let p = Math.round((diff / data.data[sensor].previous_day_count) * 100);
        if (p < 0) {movement = "increased"};
        comment = `${attackType} attacks on ${sensor} ${movement} ${Math.abs(p)} percent in the last 24 hours`;
        let node = document.createElement("DIV");
        node.classList.add("ticker__item");
      
        let textNode = document.createTextNode(comment);
        node.appendChild(textNode);
        document.getElementById('myTicker').appendChild(node); 
      }
    }
}
function showMax(data,type){

  let attackType = type;
  for (let sensor in data.data){
    let comment = '';
    
    if (data.data[sensor].maximum){
        let max = data.data[sensor].maximum;
        comment = `largest attack on ${data.data[sensor].city} sensor: ${max.count} ${attackType} attacks made through ${max.asn} in ${max.country} (last 24 hrs)`;
        addComment(comment);
    }
  }
}

function showDayCount(data,type){

  let attacktype = type;
  for (let sensor in data.data){
    let comment = '';
    let dc = data.data[sensor].day_count;
    comment = `last 24 hours total count for ${attacktype} attacks on ${data.data[sensor].city} sensor: ${dc}`;
    addComment(comment);
  }
}

function addComment(comment){
    let node = document.createElement("DIV");
    node.classList.add("ticker__item");
    let textNode = document.createTextNode(comment);
    node.appendChild(textNode);
    document.getElementById('myTicker').appendChild(node); 

}

function clearTicker(){
    let e = document.getElementById('myTicker'); 
    while (e.firstChild) {
      e.removeChild(e.firstChild);
    }
}

function compareValues(key,order='asc') {
  return function(a, b) {
    if(!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
      return 0;
    }

    const varA = (typeof a[key] === 'string') ?
      a[key].toUpperCase() : a[key];
    const varB = (typeof b[key] === 'string') ?
      b[key].toUpperCase() : b[key];
    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return (
      (order == 'desc') ? (comparison * -1) : comparison
    );
  };
}

function showTicker(){
  document.getElementById("myTicker").style.opacity = 1;
}
