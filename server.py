#!/usr/bin/python3.5
from GeoCode import GeoCode
from sanic import Sanic
from sanic.log import logger
from sanic.response import json,text
from sanic.request import RequestParameters
from redis_conn import RedisConn
from sanic_cors import CORS, cross_origin

redis_conn = RedisConn().redis_conn()

app = Sanic()
CORS(app, automatic_options=True)

geocoder = GeoCode('/home/honeycentral/GeoLite2-City.mmdb', '/home/honeycentral/GeoLite2-ASN.mmdb')


@app.route("/api/data/<honey_pot_name>", methods=['GET'])
async def get_name(request,honey_pot_name):
    
    gotten = redis_conn.get(honey_pot_name)
    return json({"data": eval(gotten)})

@app.route("/api/data/logs", methods=['POST'])
async def get_logs(request):

    type_redis = redis_conn.get(request.json['type'])
    if (type_redis):
        type_dict = eval(type_redis)
    else:
        type_dict = {}
        type_dict[request.json['device']] = {}

    type_dict[request.json['device']] = request.json
    to_save = str(type_dict)
    redis_conn.set(request.json['type'],to_save)
    
    return json({"received":"saved"})

@app.route("/api/data/stats", methods=['POST'])
async def get_stats(request):

    # day_count, previous_day_count, type, device
    type_redis = redis_conn.get(request.json['type'])
    if (type_redis):
        type_dict = eval(type_redis)
    else:
        type_dict = {}
        type_dict[request.json['device']] = {} 
    #print(request.json)
    type_dict[request.json['device']]['day_count'] = request.json['day_count']
    type_dict[request.json['device']]['previous_day_count'] = request.json['previous_day_count']
    #type_dict[request.json['device']]['maximum'] = request.json['maximum']
    # geocode maximum.ip and make a new object to add as maximum in redisDB
    if 'maximum' in request.json:
        geocoder.geocode(request.json['maximum']['ip'])
        geo_max = {
                  "country":geocoder.country, 
                  "city":geocoder.city, 
                  "latitude":geocoder.latitude, 
                  "longitude":geocoder.longitude,
                  "asn":geocoder.asn,
                  "count":request.json['maximum']['count']
              }
     
        type_dict[request.json['device']]['maximum'] = geo_max
    to_save = str(type_dict)
    redis_conn.set(request.json['type'],to_save)
    
    return json({"received":request.json})
    

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=4000, debug=True)
